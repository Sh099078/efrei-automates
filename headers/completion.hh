#pragma once

#include "automaton.hh"

bool is_complete(const Automaton&);

Automaton complete(const Automaton&);
