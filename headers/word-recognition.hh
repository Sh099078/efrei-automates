#pragma once

#include "automaton.hh"

bool recognize_word(Automaton&, std::string);
